import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def plot_pulsar_data(npdata, name='report.png'):
    """
    Plots the pulsar data including a pcolormesh plot and profile sums along both axes.

    Parameters:
    npdata (numpy.ndarray): The pulsar data to be plotted.
    """

    # Normalize the data
    npdata = (npdata - np.median(npdata)) / np.std(npdata)

    # Create a figure and set its size
    fig = plt.figure()
    fig.set_figheight(10)
    fig.set_figwidth(15)

    # Define the subplots
    ax1 = plt.subplot2grid(shape=(9, 9), loc=(0, 0), colspan=6, rowspan=6)
    ax3 = plt.subplot2grid(shape=(9, 9), loc=(0, 6), colspan=3, rowspan=6)
    ax2 = plt.subplot2grid(shape=(9, 9), loc=(6, 0), rowspan=3, colspan=6)

    # First subplot: pcolormesh of npdata spanning multiple columns and rows
    pcm = ax1.pcolormesh(npdata, vmin=1 * np.std(npdata) + np.median(npdata), vmax=10 * np.std(npdata) + np.median(npdata))
    ax1.set_xlabel('bins')
    ax1.set_ylabel('chan')

    # Second subplot: Profile sum along axis 0
    profile_0 = npdata.sum(axis=0)
    ax2.plot((profile_0 - np.median(profile_0)) / np.std(profile_0))
    ax2.set_xlabel('bins')
    ax2.set_ylabel('snr')

    # Third subplot: Profile sum along axis 1
    profile_1 = npdata.sum(axis=1)
    chan = np.arange(len(profile_1))
    ax3.plot((profile_1 - np.median(profile_1)) / np.std(profile_1), chan)
    ax3.set_xlabel('snr')
    ax3.set_ylabel('chan')
    ax3.set_ylim(0, len(profile_1))

    # Automatically adjust padding horizontally and vertically.
    plt.tight_layout()

    # Save the plot as an image file
    plt.savefig(name)

    # Display plot
#    plt.show()

# Example usage:
# Assuming npdata is already defined
# npdata = np.random.randn(100, 100)  # Example data
# plot_pulsar_data(npdata)

