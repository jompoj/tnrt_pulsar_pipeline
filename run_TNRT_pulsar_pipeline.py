import os
import subprocess
import uuid
import argparse
import logging
import sys

# Set up logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def run_command(cmd, log_file, return_output=False):
    logging.info(f"Running: {cmd}")
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    
    stdout_lines = []
    with open(log_file, "a") as lf:
        for stdout_line in iter(process.stdout.readline, b""):
            line = stdout_line.decode('utf-8')
            lf.write(line)
            if return_output:
                stdout_lines.append(line)
        stderr_lines = process.stderr.readlines()
        for stderr_line in stderr_lines:
            lf.write(stderr_line.decode('utf-8'))
    
    process.stdout.close()
    process.stderr.close()
    process.wait()
    
    if process.returncode != 0:
        logging.error(f"Command failed: {cmd}")
        sys.exit(1)
    
    if return_output:
        return ''.join(stdout_lines).strip()

def cleanup_containers():
    #user =os.environ.get('USER')
    #subprocess.call("docker stop {user}_dspsr_container {user}_filtool_container > /dev/null 2>&1 || true", shell=True)
    #subprocess.call("docker rm {user}_dspsr_container {user}_filtool_container > /dev/null 2>&1 || true", shell=True)
    subprocess.call("./tnrt_pulsar_pipeline/cleandocker", shell=True)

def cleanfil(input_file, par_file, parent_dir, work_dir, container_work_dir, suid, log_file, print_logs, user):
    try:
        # Start the container with the parent directory and working directory mounted
        try :
            run_command(f"docker run -d --name {user}_dspsr_container -v {parent_dir}:/data -v {work_dir}:/workdir vivekvenkris/vivek_pulsar_docker sleep infinity", log_file)
        except :
            cleanup_containers()
            run_command(f"docker run -d --name {user}_dspsr_container -v {parent_dir}:/data -v {work_dir}:/workdir vivekvenkris/vivek_pulsar_docker sleep infinity", log_file)

        # Step 1: Run dspsr to generate quick.ar
        run_command(f"docker exec -it -w /data {user}_dspsr_container dspsr -c 1 -D 0 -A -L 60 -t 16 -cpu 16 -O /workdir/quick /data/{os.path.basename(input_file)}", log_file)
        #run_command(f"docker exec -it -w /data dspsr_container pam -T --setnchan 512 --setnbin 1024 /workdir/quick}", log_file)
        # Step 2: Run pdv to generate profile.csv
        run_command(f"docker exec -it -w /data {user}_dspsr_container pdv -T -t /workdir/quick.ar > /{work_dir}/profile.csv", log_file)

        # Step 3: Run RFI_hunt.py outside Docker to get the RFI zap commands
        try:
            rfi_zap_commands = run_command(f"python TNRT_pulsar_pipeline/tnrt_pulsar_pipeline/RFI_hunt.py {work_dir}/profile.csv --plot {work_dir}/report.png", log_file, return_output=True)
            logging.info(f"RFI zap commands: {rfi_zap_commands}")
        except SyntaxError:
            rfi_zap_commands = run_command(f"docker exec -w /data/ -it {user}_dspsr_container python TNRT_pulsar_pipeline/tnrt_pulsar_pipeline/RFI_hunt.py /workdir/profile.csv --plot /workdir/replot.py", log_file, return_output=True)
            logging.info(f"RFI zap commands: {rfi_zap_commands}")

        except:
            rfi_zap_commands = ''
            logging.info(f"RFI zap commands: {rfi_zap_commands}")

        # Start the container for filtool
        try :
            run_command(f"docker run -d --name {user}_filtool_container -v {parent_dir}:/data -v {work_dir}:/workdir ypmen/pulsarx sleep infinity", log_file)
        except :
            cleanup_containers()
            run_command(f"docker run -d --name {user}_filtool_container -v {parent_dir}:/data -v {work_dir}:/workdir ypmen/pulsarx sleep infinity", log_file)

        # Create a symbolic link to the input file in the work directory
#        run_command(f"ln /{parent_dir}/{os.path.basename(input_file)} /{work_dir}/{os.path.basename(input_file)}", log_file)
        
        # Step 4: Run filtool with the output of RFI_hunt.py script
        #os.chdir(work_dir)
        run_command(f"docker exec -it -w /data {user}_filtool_container filtool --baseline 10 --scloffs --zero_off -t 16 -f /data/{os.path.basename(input_file)} -o {suid} -v --psrfits  -z kadaneF 4 8 {rfi_zap_commands}", log_file)
#        run_command(f"docker exec -it -w /data filtool_container filtool -t 32 -f /data/{os.path.basename(input_file)} -o {suid} -v --psrfits  -z zdot", log_file)
        run_command(f"mv /{parent_dir}/{suid}*.fil /{work_dir}", log_file)
        # Remove intermediate files
#        os.remove(f"{work_dir}/quick.ar")
#        os.remove(f"{work_dir}/profile.csv")

        # Step 5: Run dspsr with cleaned data
      #  run_command(f"docker exec -it -w /data dspsr_container dspsr -cpu 32 -t 32 -A -L 30 -O /workdir/out -E /data/{os.path.basename(par_file)} /workdir/*.fil", log_file)

        # Stop and remove both containers
        cleanup_containers()

        logging.info("Cleanfil complete. Intermediate results are in {}".format(work_dir))

        if not os.path.exists(f"{parent_dir}/{par_file}"):
            logging.info(f"Parameter file not found: {parent_dir}/{par_file}")
            logging.info("Stopping after cleanfil.")
            return False

        return True
    except Exception as e:
        logging.error(f"Error during cleanfil: {e}")
        cleanup_containers()
        sys.exit(1)

def prep_archive(work_dir, par_file, parent_dir, container_work_dir, log_file,user, cal):
    try:
        unique_id = os.path.basename(work_dir)

        # Start the container with the parent directory mounted
        #run_command(f"docker run -d --name dspsr_container -v {parent_dir}:/data vivekvenkris/vivek_pulsar_docker sleep infinity", log_file)
        try :
            run_command(f"docker run -d --name {user}_dspsr_container -v {parent_dir}:/data -v {work_dir}:/workdir vivekvenkris/vivek_pulsar_docker sleep infinity", log_file)
        except :
            pass
        run_command(f"docker exec -it -w /data {user}_dspsr_container sh -c 'echo -967128.775 5960055.199 2049264.045 TNRT       tnrt >> /home/psr/software/tempo2/T2runtime/observatory/observatories.dat'", log_file)
        # Step 5: Run dspsr with cleaned data
        if cal==True :
            run_command(f"docker exec -it -w /data {user}_dspsr_container dspsr -cpu 16 -t 16 -A -L 10 -O /workdir/out -c 1 -D 0 /workdir/*.fil", log_file)
        elif os.path.exists(f"{parent_dir}/{par_file}"):
            run_command(f"docker exec -it -w /data {user}_dspsr_container dspsr -cpu 16 -t 16 -A -L 10 -O /workdir/out -E /data/{os.path.basename(par_file)} /workdir/*.fil", log_file)
        else :
            run_command(f"docker exec -it -w /data {user}_dspsr_container dspsr -cpu 16 -t 16 -A -L 10 -O /workdir/out -c 1 -D 0 /workdir/*.fil", log_file)

        # Step 6: Edit the archive file using psredit with the new arguments
        run_command(f"docker exec -it -w /workdir {user}_dspsr_container psredit -c site=TNRT -m out.ar", log_file)
        if cal!=True :
            run_command(f"docker exec -it -w /workdir {user}_dspsr_container pam --ephver tempo -E /data/{os.path.basename(par_file)} -m out.ar", log_file)
        run_command(f"docker exec -it -w /workdir {user}_dspsr_container pav -DFT -g profile.png/png /workdir/out.ar", log_file)
        run_command(f"docker exec -it -w /workdir {user}_dspsr_container pav -dGT -g freq-phase.png/png /workdir/out.ar", log_file)
        run_command(f"docker exec -it -w /workdir {user}_dspsr_container pav -dRF -g time-phase.png/png /workdir/out.ar", log_file)
    #    run_command(f"docker exec -it -w /workdir dspsr_container pav -dF -g profile.png/png /workdir/out.ar", log_file)
        # Stop and remove the container
        cleanup_containers()

        logging.info("Processing complete. Results are in {}".format(work_dir))
    except Exception as e:
        logging.error(f"Error during prep_archive: {e}")
        cleanup_containers()
        sys.exit(1)

def test_function():
    try:
        # Define minimal test inputs
        test_input_file = "test.sf"
        test_par_file = "test.par"
        test_parent_dir = os.getcwd()

        # Generate test input file and parameter file
        with open(test_input_file, 'w') as f:
            f.write("TEST DATA")
        with open(test_par_file, 'w') as f:
            f.write("TEST PARAMETER")

        # Get the source name (mock for test)
        source_name = "test_source"
        unique_id = str(uuid.uuid4())[:3]
        work_dir = os.path.join(test_parent_dir, f"{source_name}_{unique_id}")
        os.makedirs(work_dir)

        container_work_dir = f"/data/{source_name}_{unique_id}"
        suid = f"{source_name}_{unique_id}"
        log_file = os.path.join(work_dir, 'pipeline.log')

        if cleanfil(test_input_file, test_par_file, test_parent_dir, work_dir, container_work_dir, suid, log_file, False):
            prep_archive(work_dir, test_par_file, test_parent_dir, container_work_dir, log_file)

        # Clean up test files
        os.remove(test_input_file)
        os.remove(test_par_file)
        os.rmdir(work_dir)
        logging.info("Test function executed successfully.")
        cleanup_containers()

    except Exception as e:
        logging.error(f"Error during test function: {e}")
        cleanup_containers()
        sys.exit(1)

def main():
    parser = argparse.ArgumentParser(description="Process pipeline")
    parser.add_argument('-i', '--input', required=True, help='Input file')
    parser.add_argument('-p', '--par', help='Parameter file')
    parser.add_argument('-d', '--dir', required=True, help='Parent directory')
    parser.add_argument('-l', '--logs', action='store_true', help='Print logs of each step')
    parser.add_argument('--test', action='store_true', help='Run the test function')
    parser.add_argument('--cal', action='store_true', help='Also fold as a calibrator')
    args = parser.parse_args()

    if args.test:
        test_function()
        return

    input_file = args.input
    par_file = args.par
    parent_dir = args.dir
    print_logs = args.logs
    cal = args.cal
    user = user =os.environ.get('USER')
    print(user)
    # Get the source name
    try:
        source_name = run_command(f"docker run -it --rm -v {os.getcwd()}:/data vivekvenkris/vivek_pulsar_docker vap -c name,stt_smjd,stt_imjd /data/{input_file} | tail -1 | awk '{{print $2}}'", str(user)+"param.log",return_output=True)
        sec = run_command(f"docker run -it --rm -v {os.getcwd()}:/data vivekvenkris/vivek_pulsar_docker vap -c name,stt_smjd,stt_imjd /data/{input_file} | tail -1 | awk '{{print $3}}'", str(user)+"param.log",return_output=True)
        mjd = run_command(f"docker run -it --rm -v {os.getcwd()}:/data vivekvenkris/vivek_pulsar_docker vap -c name,stt_smjd,stt_imjd /data/{input_file} | tail -1 | awk '{{print $4}}'", str(user)+"param.log",return_output=True)
        logging.info(f"Source name: {source_name}")
        print(source_name,sec,mjd)
    except Exception as e:
        logging.error(f"Error retrieving file parametres, please check your file: {e}")
        sys.exit(1)

    # Define unique identifier and working directory
    unique_id = str(uuid.uuid4())[:3]
    work_dir = os.path.join(parent_dir, f"{source_name}_{mjd}_{sec}_{unique_id}")
    os.makedirs(work_dir)
    suid = f"{source_name}_{mjd}_{sec}_{unique_id}"
    container_work_dir = f"/data/{source_name}_{mjd}_{sec}_{unique_id}"
#    print(suid,container_work_dir,work_dir)
    log_file = os.path.join(work_dir, 'pipeline.log')

    cleanfil(input_file, par_file, parent_dir, work_dir, container_work_dir, suid ,log_file, print_logs, user)
    prep_archive(work_dir, par_file, parent_dir, container_work_dir, log_file, user, cal)

if __name__ == "__main__":
    main()

