import argparse
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import kurtosis, skew
import numpy as np
from plot_pulsar_data import plot_pulsar_data

def calculate_mean_and_sigma(array):
    mean = np.mean(array)
    sigma = np.std(array)
    return mean, sigma

def group_and_print_bad_channels(channels):
    grouped = []
    current_group = [channels[0]]

    for i in range(1, len(channels)):
        if channels[i] == channels[i - 1] + 1:
            current_group.append(channels[i])
        else:
            if len(current_group) > 2:
                grouped.append((current_group[0], current_group[-1]))
            current_group = [channels[i]]

    if len(current_group) > 2:
        grouped.append((current_group[0], current_group[-1]))

    for group in grouped:
        print "zap {} {}".format(group[0] * 1.7578125 + 900, group[1] * 1.7578125 + 900),

def find_positions_above_sigma(array, x):
    """
    Find positions of elements in the array that are greater than x times the sigma above the mean.

    Parameters:
    array (np.ndarray): Input array of numerical values.
    x (float): Number of sigmas to use as the threshold.

    Returns:
    np.ndarray: Array of indices of elements greater than x times sigma above the mean.
    """
    mean, sigma = calculate_mean_and_sigma(array)
    threshold = mean + x * sigma
    positions = np.where(array > threshold)[0]
    return positions

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process and analyze profile data")
    parser.add_argument('input_file', type=str, help='Input CSV file')
    parser.add_argument('--dims', type=str, default='512,1024', help='Array dimensions as rows,cols (default: 512,1024)')
    parser.add_argument('--std_thresh', type=float, default=3.0, help='Threshold for standard deviation (default: 3.0)')
    parser.add_argument('--skew_thresh', type=float, default=1.5, help='Threshold for skewness (default: 1.5)')
    parser.add_argument('--kurt_thresh', type=float, default=0.75, help='Threshold for kurtosis (default: 0.75)')
    parser.add_argument('--plot', type=str, default='report.png', help='name of the plot to inspect (default: report.png)')
    args = parser.parse_args()
    
    # Parse the dimensions argument
    dims = tuple(map(int, args.dims.split(',')))

    # Read data from the input CSV file
    data = pd.read_csv(args.input_file, delimiter=' ', skiprows=1, names=['sub', 'chan', 'bin', 'amp'])

    # Reshape the 'amp' column into the specified dimensions
    amp_array = data['amp'].values.reshape(dims)
    try :
        plot_pulsar_data(np.asarray(amp_array), args.plot)
    except :
        pass
    sum_ = amp_array.sum(axis=1)
    std_ = amp_array.std(axis=1)
    skew_ = skew(amp_array, axis=1)
    kurtosis_ = kurtosis(amp_array, axis=1)

    bad_std = find_positions_above_sigma(std_, args.std_thresh)
    bad_skew = find_positions_above_sigma(skew_, args.skew_thresh)
    bad_kur = find_positions_above_sigma(kurtosis_, args.kurt_thresh)

    union_array = np.union1d(bad_std, np.union1d(bad_skew, bad_kur))

    group_and_print_bad_channels(union_array)

