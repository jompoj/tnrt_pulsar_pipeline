
# Pulsar Data Processing Pipeline

This repository contains a pipeline for processing pulsar data using Docker containers. The pipeline includes several steps for cleaning and processing the data, leveraging tools such as `dspsr`, `pdv`, and `filtool`.

This README file is generate automatically from CHATGPT. Please contact jompoj@mpifr-bonn.mpg.de before using this. 


## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Usage](#usage)
  - [Running the Pipeline](#running-the-pipeline)
  - [Running Tests](#running-tests)
- [Project Structure](#project-structure)
- TODO
- [Contributing](#contributing)
- [License](#license)

## Prerequisites

- Docker
- Python 3.6 or higher
- Pip (Python package installer)

## Installation

1. Clone the repository:
    \`\`\`sh
    git clone https://github.com/yourusername/pulsar-data-processing-pipeline.git
    cd pulsar-data-processing-pipeline
    \`\`\`

2. Install the required Python packages:
    \`\`\`sh
    pip install -r requirements.txt
    \`\`\`

## Usage

### Running the Pipeline

To run the pipeline, you need to provide an input file, a parameter file, and a parent directory. Use the following command:

\`\`\`sh
python pipeline.py -i <input_file> -p <par_file> -d <parent_directory>
\`\`\`

#### Arguments:

- \`-i, --input\`: Input file (required)
- \`-p, --par\`: Parameter file (required)
- \`-d, --dir\`: Parent directory (required)
- \`-l, --logs\`: Print logs of each step (optional)
- \`--test\`: Run the test function (optional)

### Running Tests

To run the test function which includes minimal test inputs, use the \`--test\` flag:

\`\`\`sh
python pipeline.py --test
\`\`\`

## Project Structure

\`\`\`
pulsar-data-processing-pipeline/
├── pipeline.py         # Main script for the pipeline
├── requirements.txt    # Python dependencies
├── README.md           # This file
└── .gitignore          # Git ignore file
\`\`\`



## TODO 
Make sure that only one docker for each package is running at the same time 
Get observation length information from psredit 
Get CPU cores  
Allow clean mode, fold mode, TOAs mode ,and SP search mode 


## Contributing

Contributions are welcome! Please open an issue or submit a pull request for any improvements or bug fixes.

1. Fork the repository
2. Create a new branch (\`git checkout -b feature-branch\`)
3. Make your changes
4. Commit your changes (\`git commit -am 'Add some feature'\`)
5. Push to the branch (\`git push origin feature-branch\`)
6. Open a Pull Request

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
